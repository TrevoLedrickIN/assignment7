import { Component, EventEmitter, Input, Output } from '@angular/core'
import { Pokemon } from '../../models/pokemon.model'

@Component({
	selector: 'app-pokemon-list',
	templateUrl: './pokemon-list.component.html',
	styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent {

	@Input() pokemon: Pokemon[] = []
	@Input() buttonName: string = ""
	@Output() clicked: EventEmitter<Pokemon> = new EventEmitter();

	constructor() { }

	public onPokemonClick(pokemon: Pokemon): void {
		this.clicked.emit(pokemon);
	}

}
