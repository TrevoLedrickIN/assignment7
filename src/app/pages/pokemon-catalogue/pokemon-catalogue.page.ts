import { Component } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonService } from 'src/app/services/pokemon.service';
import { UserService } from 'src/app/services/user.service';

@Component({
	selector: 'app-pokemon-catalogue',
	templateUrl: './pokemon-catalogue.page.html',
	styleUrls: ['./pokemon-catalogue.page.css'],
})
export class PokemonCataloguePage {
	public pokemon: Pokemon[] = []

	constructor(
		private readonly userService: UserService,
		private readonly pokemonservice: PokemonService
	) { }

	public ngOnInit(): void {
		this.pokemon = this.pokemonservice.getPokemon()
	}

	public handlePokemonClick(pokemon: Pokemon) {
		this.userService.addPokemon(pokemon)
		this.pokemonservice.collectPokemon(pokemon)
		alert(`You caught ${pokemon.name}`)
	}

	public loadMore() {
		this.pokemonservice.fetchPokemon()
	}
}