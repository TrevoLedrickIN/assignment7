import { Component, OnInit } from '@angular/core'
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonService } from 'src/app/services/pokemon.service';
import { UserService } from 'src/app/services/user.service';

@Component({
	selector: 'app-trainer',
	templateUrl: './trainer.page.html',
	styleUrls: ['./trainer.page.css']
})
export class TrainerPage implements OnInit {
	public pokemon: Pokemon[] = []

	constructor(
		private readonly userService: UserService,
		private readonly pokemonService: PokemonService
	) { }

	public ngOnInit(): void {
		this.pokemon = this.userService.getPokemon()
	}

	public handlePokemonClick(pokemon: Pokemon) {
		confirm(`Are you sure you would like to release ${pokemon.name}?`)
		this.pokemonService.releasePokemon(pokemon)
		this.userService.removePokemon(pokemon)
		this.pokemon = this.userService.getPokemon()
	}
}
