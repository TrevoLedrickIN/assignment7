import { Component } from '@angular/core'
import { NgForm } from '@angular/forms'
import { Router } from '@angular/router'
import { User } from 'src/app/models/user.model'
import { UserService } from 'src/app/services/user.service'
import { LoginService } from '../../services/login.service'

@Component({
	selector: 'app-landing',
	templateUrl: './landing.page.html',
	styleUrls: ['./landing.page.css'],
})
export class LandingPage {
	constructor(
		private readonly router: Router,
		private readonly loginService: LoginService,
		private readonly userService: UserService) { }

	public onSubmit(form: NgForm): void {
		const { username } = form.value
		if (username.trim() === '') {
			throw new Error("That's not a username, Hackerman!")
		}

		this.loginService.login(username).subscribe({
			next: (user: User) => {
				this.userService.setUser(user)
				this.router.navigateByUrl("/pokemon-catalogue")
			},
		})
	}
}
