export interface Pokemon {
	url: string
	name: string
	collected?: boolean
}
