import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { User } from '../models/user.model'

const USER_STORAGE_KEY = 'user'
const { userAPI, apiKey } = environment;

@Injectable({
	providedIn: 'root',
})
export class UserService {
	public user: User | undefined

	constructor(
		private readonly http: HttpClient
	) {
		const storedUser = sessionStorage.getItem(USER_STORAGE_KEY)
		if (!storedUser) {
			return
		}

		try {
			const json = JSON.parse(storedUser)
			this.user = json as User
		} catch (e) {
			sessionStorage.removeItem(USER_STORAGE_KEY)
		}
	}

	public getUser(): User | undefined {
		return this.user
	}

	public setUser(user: User): void {
		this.user = user
		sessionStorage.setItem(USER_STORAGE_KEY, JSON.stringify(user))
	}

	public getPokemon(): Pokemon[] {
		return this.user?.pokemon || []
	}

	public addPokemon(pokemon: Pokemon) {
		let u = this.user

		if (!u)
			return

		let mon = u.pokemon.find(m => m.name === pokemon.name)
		if (mon !== undefined)
			return

		u.pokemon.push(pokemon)
		this.patchTrainer(u).subscribe({
			next: (user: User) => {
				this.setUser(user)
			},
		})
	}

	private patchTrainer(user: User): Observable<User> {
		const headers = new HttpHeaders({
			"Content-Type": "application/json",
			"x-api-key": apiKey
		})

		return this.http.patch<User>(`${userAPI}/${user.id}`, user, { headers })
	}

	public removePokemon(pokemon: Pokemon): void {
		let u = this.user

		if (!u)
			return

		let index = u.pokemon.findIndex(m => m.name === pokemon.name)
		if (index == -1)
			return

		u.pokemon.splice(index, 1)
		this.putTrainer(u).subscribe({
			next: (user: User) => {
				this.setUser(user)
			},
		})
	}

	private putTrainer(user: User): Observable<User> {
		const headers = new HttpHeaders({
			"Content-Type": "application/json",
			"x-api-key": apiKey
		})

		return this.http.put<User>(`${userAPI}/${user.id}`, user, { headers })
	}
}
