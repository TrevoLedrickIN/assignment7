import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { environment } from 'src/environments/environment'
import { Pokemon } from '../models/pokemon.model'
import { UserService } from './user.service'

const { pokemonAPI } = environment;
const POKEMON_STORAGE_KEY = "pokemon"

export interface PokemonResult {
	count: number
	next: string
	results: Pokemon[]
	previous: string
}


@Injectable({
	providedIn: 'root'
})
export class PokemonService {
	public pokemonList: Pokemon[] = []

	constructor(
		private readonly http: HttpClient,
		private readonly userService: UserService
	) {
		const storedPokemon = sessionStorage.getItem(POKEMON_STORAGE_KEY)
		if (!storedPokemon) {
			this.fetchPokemon()
			return
		}

		try {
			const json = JSON.parse(storedPokemon)
			this.pokemonList = json as Pokemon[]
		} catch (e) {
			sessionStorage.removeItem(POKEMON_STORAGE_KEY)
		}
	}

	public setPokemon(): void {
		sessionStorage.setItem(POKEMON_STORAGE_KEY, JSON.stringify(this.pokemonList))
	}

	public getPokemon(): Pokemon[] {
		return this.pokemonList
	}

	public fetchPokemon(): void {
		this.http.get<PokemonResult>(`${pokemonAPI}??limit=20&offset=${this.pokemonList.length}}`)
			.subscribe({
				next: (pokemonResult: PokemonResult) => {
					for (let mon of pokemonResult.results) {
						if (this.userService.getPokemon().find(m => m.name === mon.name) !== undefined)
							mon.collected = true
						this.pokemonList.push(mon)
					}
					this.setPokemon()
				},
			})
	}

	public collectPokemon(pokemon: Pokemon) {
		for (const mon of this.pokemonList) {
			if (mon == pokemon)
				mon.collected = true
		}
		this.setPokemon()

	}
	public releasePokemon(pokemon: Pokemon) {
		let mon = this.pokemonList.find(m => m.name === pokemon.name)
		if (mon !== undefined)
			mon.collected = false

		this.setPokemon()
	}
}
