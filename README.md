# Assignment7

## Name
Pokémon Catalogue App

## Description
A simple application using Angular Typescript to display a pokémon catalogue with collected pokémon by the trainer. This website also makes fetching calls from an api. Shows a Login page which redirect to Pokémon Catalogue Page and have the option to view the Trainer Page.

## Contributors
- Trevo Ledrick
- Anders Thengs Kristensen

## Visuals
![pokemonCatalogue.png](./pokemonCatalogue.png)
![trainerPage.png](./trainerPage.png)
